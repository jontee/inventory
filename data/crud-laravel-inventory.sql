-- phpMyAdmin SQL Dump
-- version 4.5.1
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Nov 18, 2016 at 04:26 PM
-- Server version: 10.1.13-MariaDB
-- PHP Version: 5.6.21

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";

--
-- Database: `crud`
--

-- --------------------------------------------------------

--
-- Table structure for table `bottle_debtors`
--

CREATE TABLE `bottle_debtors` (
  `id` int(10) UNSIGNED NOT NULL,
  `users_id` int(11) NOT NULL,
  `transaction_ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `item_id` int(11) NOT NULL,
  `d_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `error_type` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `qty_bottle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `amount_paid` decimal(10,2) NOT NULL,
  `is_rgb_content` tinyint(4) DEFAULT '0',
  `is_cleared` tinyint(4) DEFAULT '0',
  `comment` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bottle_debtors`
--

INSERT INTO `bottle_debtors` (`id`, `users_id`, `transaction_ref`, `item_id`, `d_name`, `error_type`, `qty_bottle`, `amount_paid`, `is_rgb_content`, `is_cleared`, `comment`, `created_at`, `updated_at`) VALUES
(1, 1, '25579e81107688485ijjTFmnanTnLdVd', 3, 'Natural', 'Natural', '2', '200.00', 1, 0, '2 bottles found broken in store', '2016-11-14 19:19:10', '2016-11-14 19:19:10'),
(2, 1, '25579e811133f7630oRbaFfWDVBYhkhR', 5, 'Ola', 'Error', '7', '700.00', 1, 0, 'Ola broke 7 bottkes', '2016-11-14 19:26:02', '2016-11-14 19:26:02'),
(3, 1, '25579e811b910f4c2gbBLLGemgCSXWbo', 4, 'Ola', 'Error', '5', '250.00', NULL, 0, '', '2016-11-18 13:10:48', '2016-11-18 13:10:48');

-- --------------------------------------------------------

--
-- Table structure for table `bottle_sales`
--

CREATE TABLE `bottle_sales` (
  `id` int(10) UNSIGNED NOT NULL,
  `store_users_id` int(11) NOT NULL,
  `sales_users_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `transaction_ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `d_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `s_name` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `c_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `qty_bottle_content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price_unit` decimal(10,2) NOT NULL,
  `price_total` decimal(10,2) NOT NULL,
  `comment` text COLLATE utf8_unicode_ci,
  `is_confirmed` tinyint(4) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `bottle_sales`
--

INSERT INTO `bottle_sales` (`id`, `store_users_id`, `sales_users_id`, `item_id`, `transaction_ref`, `d_name`, `s_name`, `c_name`, `qty_bottle_content`, `price_unit`, `price_total`, `comment`, `is_confirmed`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 3, '25579e811afee76d8RGLhhajeghCgmFL', 'Ibk Stores', 'Ibk Stores', 'Test', '2', '1000.00', '2000.00', 'Just testing', 0, '2016-11-14 18:16:48', '2016-11-14 18:16:48'),
(2, 1, 0, 4, '25579e8115cf81713CajYqbdeBLLZaEb', 'Ola', NULL, 'Individual', '3', '1100.00', '3300.00', '', 0, '2016-11-18 13:23:42', '2016-11-18 13:23:42');

-- --------------------------------------------------------

--
-- Table structure for table `carts`
--

CREATE TABLE `carts` (
  `id` int(10) UNSIGNED NOT NULL,
  `store_users_id` int(11) DEFAULT NULL,
  `sales_users_id` int(11) DEFAULT NULL,
  `item_id` int(11) NOT NULL,
  `transaction_ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cart_session` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `d_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `i_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `c_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_rgb` tinyint(4) NOT NULL,
  `qty_content` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty_bottle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `returned_qty` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `returned_bottle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price_unit` decimal(10,2) NOT NULL,
  `price_total` decimal(10,2) NOT NULL,
  `is_confirmed` tinyint(4) NOT NULL DEFAULT '0',
  `deleted` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `carts`
--

INSERT INTO `carts` (`id`, `store_users_id`, `sales_users_id`, `item_id`, `transaction_ref`, `cart_session`, `d_name`, `i_name`, `c_name`, `is_rgb`, `qty_content`, `qty_bottle`, `qty`, `returned_qty`, `returned_bottle`, `price_unit`, `price_total`, `is_confirmed`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 1, NULL, 4, '25579e81139293b9ckakZmLidWqnCRmj', '1478755293', 'Ola', '', 'Test', 1, '16', '30', '5', NULL, NULL, '1200.00', '6000.00', 0, 0, '2016-11-10 04:38:01', '2016-11-10 04:38:01'),
(3, 1, NULL, 4, '25579e8117d26b4e9WkjECioEVSeXnYD', '1478945298', 'Ola', '', 'Test', 1, '11', '30', '2', NULL, NULL, '1200.00', '2400.00', 1, 0, '2016-11-12 11:18:54', '2016-11-12 11:47:45'),
(4, 1, NULL, 4, '25579e8117d26b4e9WkjECioEVSeXnYD', '1478945298', 'Ola', '', 'Test', 1, '9', '30', '5', NULL, NULL, '1200.00', '6000.00', 1, 0, '2016-11-12 11:19:11', '2016-11-12 11:47:45'),
(5, 1, NULL, 14, '25579e8117d26b4e9WkjECioEVSeXnYD', '1478955289', 'Ola', '', 'Test', 0, NULL, NULL, '6', NULL, NULL, '1000.00', '6000.00', 1, 0, '2016-11-12 13:42:45', '2016-11-12 13:46:04'),
(7, NULL, 1, 5, '25579e81159d28d9bfpZLidSXZChLmjW', '1479037907', 'Individual', '', 'Test', 1, '20', '30', '4', NULL, NULL, '1200.00', '4800.00', 1, 0, '2016-11-13 11:14:35', '2016-11-13 11:16:30'),
(8, NULL, 1, 6, '25579e81159d28d9bfpZLidSXZChLmjW', '1479037907', 'Individual', '', 'Test', 0, NULL, NULL, '6', NULL, NULL, '1200.00', '7200.00', 1, 0, '2016-11-13 11:14:56', '2016-11-13 11:16:30'),
(9, NULL, 1, 14, '25579e81159d28d9bfpZLidSXZChLmjL', '1479040124', 'Individual', '', 'Test', 0, NULL, NULL, '3', NULL, NULL, '1000.00', '3000.00', 1, 0, '2016-11-13 11:28:59', '2016-11-13 11:29:13'),
(11, NULL, 1, 11, '25579e8117d26b4e9EAUqfokeAnqYdXj', '1479042501', 'Individual', '', 'Test', 0, NULL, NULL, '2', NULL, NULL, '1200.00', '2400.00', 1, 0, '2016-11-13 12:08:30', '2016-11-13 12:22:29'),
(12, NULL, 1, 4, '25579e81158c0b166bbaTdhiUZmToFbF', '1479047112', 'Individual', '', 'Test', 1, '5', '34', '2', NULL, NULL, '1200.00', '2400.00', 1, 0, '2016-11-13 13:25:31', '2016-11-13 13:25:40'),
(13, 1, NULL, 5, '25579e81183da3647oekbEgbWWdVLmgg', '1479275803', 'Ola', '', 'Test', 1, '15.708', '30', '4', NULL, NULL, '1200.00', '4800.00', 1, 0, '2016-11-16 05:39:40', '2016-11-16 05:42:39'),
(14, NULL, 1, 4, '25579e811d4fcff62nqYmLCXAeqUApRh', '1479277583', 'Individual', '', 'Test', 1, '6', '34', '1', NULL, NULL, '1200.00', '1200.00', 1, 0, '2016-11-16 05:46:46', '2016-11-16 05:48:57'),
(15, 1, NULL, 3, '25579e8117d26b4e9qjhcobeohpRaidD', '1479420510', 'Ola', '', 'Test', 1, '30', '18', '2', NULL, NULL, '1200.00', '2400.00', 1, 0, '2016-11-17 21:08:44', '2016-11-17 21:09:58'),
(16, 1, NULL, 5, '25579e8117d26b4e9qjhcobeohpRaidD', '1479420510', 'Ola', '', 'Test', 1, '11.708', '30', '3', NULL, NULL, '1200.00', '3600.00', 1, 0, '2016-11-17 21:09:06', '2016-11-17 21:09:58'),
(17, 1, NULL, 11, '25579e8117d26b4e9qjhcobeohpRaidD', '1479420510', 'Ola', '', 'Test', 0, NULL, NULL, '4', NULL, NULL, '1200.00', '4800.00', 1, 0, '2016-11-17 21:09:46', '2016-11-17 21:09:58'),
(18, 1, NULL, 14, '25579e8116db6b86aapTTCfiBTDbgSRe', '1479475564', 'Ola', '', 'Test', 0, NULL, NULL, '3', NULL, NULL, '1000.00', '3000.00', 1, 0, '2016-11-18 12:26:22', '2016-11-18 12:30:13'),
(19, 1, NULL, 7, '25579e8116db6b86aapTTCfiBTDbgSRe', '1479475564', 'Ola', '', 'Test', 0, NULL, NULL, '4', NULL, NULL, '1200.00', '4800.00', 1, 0, '2016-11-18 12:26:49', '2016-11-18 12:30:13'),
(20, NULL, 1, 8, '25579e8112a30f393RDaagBkikAmBDcS', '1479476173', 'Individual', '', 'Test', 0, NULL, NULL, '5', NULL, NULL, '1200.00', '6000.00', 1, 0, '2016-11-18 12:36:33', '2016-11-18 12:38:00'),
(21, NULL, 1, 5, '25579e8112a30f393RDaagBkikAmBDcS', '1479476173', 'Individual', '', 'Test', 1, '9.708', '36', '2', NULL, NULL, '1200.00', '2400.00', 1, 0, '2016-11-18 12:37:12', '2016-11-18 12:38:00');

-- --------------------------------------------------------

--
-- Table structure for table `cart_purchases`
--

CREATE TABLE `cart_purchases` (
  `id` int(10) UNSIGNED NOT NULL,
  `store_users_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `transaction_ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `cart_session` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `s_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `i_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_rgb` tinyint(4) DEFAULT '0',
  `is_bottle` tinyint(4) DEFAULT '0',
  `no_exchange` tinyint(4) DEFAULT '0',
  `qty_bottle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `qty` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price_unit` decimal(10,2) NOT NULL,
  `price_total` decimal(10,2) NOT NULL,
  `is_confirmed` tinyint(4) NOT NULL,
  `deleted` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cart_purchases`
--

INSERT INTO `cart_purchases` (`id`, `store_users_id`, `item_id`, `transaction_ref`, `cart_session`, `s_name`, `i_name`, `is_rgb`, `is_bottle`, `no_exchange`, `qty_bottle`, `qty`, `price_unit`, `price_total`, `is_confirmed`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 1, 3, '25579e8112ce6c810VCjohhndfCTeaTj', '1479103503', 'Ibk Stores', '', 1, 0, 1, '', '5', '1100.00', '5500.00', 1, 0, '2016-11-14 07:15:47', '2016-11-14 07:48:36'),
(4, 1, 14, '25579e8112ce6c810VCjohhndfCTeaTj', '1479103503', 'Ibk Stores', '', 0, 0, NULL, '', '7', '1000.00', '7000.00', 1, 0, '2016-11-14 07:27:23', '2016-11-14 07:48:36'),
(5, 1, 3, '25579e8114114723aWiTqYZieEaFGFhb', '1479114400', 'Ibk Stores', '', 1, 0, 1, '', '3', '1200.00', '3600.00', 1, 0, '2016-11-14 08:07:43', '2016-11-14 08:10:40'),
(6, 1, 4, '25579e811e197cfd5BEdAfFkmRdYnnhC', '1479114727', 'Ibk Stores', '', 1, 0, 0, '', '3', '1200.00', '3600.00', 1, 0, '2016-11-14 08:12:26', '2016-11-14 08:14:58'),
(7, 1, 3, '25579e811a88e8808miVTXALLoGDDnpa', '1479114915', 'Ibk Stores', '', 1, 0, 1, '', '2', '1200.00', '2400.00', 1, 0, '2016-11-14 08:16:49', '2016-11-14 08:17:22'),
(8, 1, 10, '25579e811a88e8808miVTXALLoGDDnpa', '1479114915', 'Ibk Stores', '', 0, 0, NULL, '', '3', '1200.00', '3600.00', 1, 0, '2016-11-14 08:17:11', '2016-11-14 08:17:22'),
(9, 1, 3, '25579e8119a92ddcdBGFkCkpAUpoDCAV', '1479279183', 'Ibk Stores', '', 1, 0, 1, '', '2', '1200.00', '2400.00', 1, 0, '2016-11-16 05:55:16', '2016-11-16 05:57:16'),
(10, 1, 5, '25579e811e5311062dnWaXRAFXhmfABa', '1479476609', 'Ibk Stores', '', 1, 0, 1, '', '12', '1200.00', '14400.00', 1, 0, '2016-11-18 12:44:18', '2016-11-18 12:47:46'),
(11, 1, 4, '25579e811e5311062dnWaXRAFXhmfABa', '1479476609', 'Ibk Stores', '', 1, 0, 1, '', '4', '1200.00', '4800.00', 1, 0, '2016-11-18 12:45:09', '2016-11-18 12:47:46'),
(12, 1, 14, '25579e8111f3f7b95qVjDajCLBpZnTAR', '1479477319', 'Ibk Stores', '', 0, 0, NULL, '', '2', '1000.00', '2000.00', 1, 0, '2016-11-18 12:55:37', '2016-11-18 12:55:59'),
(13, 1, 10, '25579e811d2c2f186BqbTBUFqEiqqEXF', '1479477535', 'Ibk Stores', '', 0, 0, NULL, '', '4', '1200.00', '4800.00', 1, 0, '2016-11-18 12:59:12', '2016-11-18 13:00:15'),
(14, 1, 5, '25579e811d2c2f186BqbTBUFqEiqqEXF', '1479477535', 'Ibk Stores', '', 1, 0, 1, '', '3', '1200.00', '3600.00', 1, 0, '2016-11-18 12:59:49', '2016-11-18 13:00:15'),
(15, 1, 6, '25579e8116db6b86aGjRobCUBVjbUZEi', '1479480071', 'Ibk Stores', '', 0, 0, NULL, '', '2', '1200.00', '2400.00', 1, 0, '2016-11-18 13:41:41', '2016-11-18 13:42:35'),
(16, 1, 4, '25579e811d4fcff62XfGUpgATqoaRWST', '1479480310', 'Ibk Stores', '', 1, 0, 1, '', '2', '1200.00', '2400.00', 1, 0, '2016-11-18 13:45:34', '2016-11-18 13:47:16');

-- --------------------------------------------------------

--
-- Table structure for table `categories`
--

CREATE TABLE `categories` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `categories`
--

INSERT INTO `categories` (`id`, `name`, `created_at`, `updated_at`) VALUES
(1, 'Coca-Cola', NULL, NULL),
(2, 'Pepsi', NULL, NULL),
(3, 'Mowa', NULL, NULL),
(4, 'Others', NULL, NULL);

-- --------------------------------------------------------

--
-- Table structure for table `customers`
--

CREATE TABLE `customers` (
  `id` int(10) UNSIGNED NOT NULL,
  `c_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `customers`
--

INSERT INTO `customers` (`id`, `c_name`, `phone`, `address`, `created_at`, `updated_at`) VALUES
(1, 'Test', '2348038021699', 'Abeokuta', '2016-10-27 16:24:23', '2016-10-27 16:24:23');

-- --------------------------------------------------------

--
-- Table structure for table `drivers`
--

CREATE TABLE `drivers` (
  `id` int(10) UNSIGNED NOT NULL,
  `d_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `drivers`
--

INSERT INTO `drivers` (`id`, `d_name`, `phone`, `address`, `created_at`, `updated_at`) VALUES
(1, 'Ola', '2348038061677', 'ekiti', '2016-11-08 09:52:02', '2016-11-08 09:52:02');

-- --------------------------------------------------------

--
-- Table structure for table `items`
--

CREATE TABLE `items` (
  `id` int(10) UNSIGNED NOT NULL,
  `users_id` int(11) NOT NULL,
  `categories_id` int(11) NOT NULL,
  `i_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_rgb` tinyint(4) NOT NULL,
  `qty` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `qty_bottle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `qty_content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price_unit` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `items`
--

INSERT INTO `items` (`id`, `users_id`, `categories_id`, `i_name`, `is_rgb`, `qty`, `qty_bottle`, `qty_content`, `price_unit`, `created_at`, `updated_at`) VALUES
(3, 1, 1, 'RGB-BIG', 1, '', '20', '28', '1200.00', '2016-10-24 23:50:10', '2016-11-18 12:07:32'),
(4, 1, 1, 'RGB-BIG', 1, '', '24.792', '11', '1200.00', '2016-10-24 23:52:54', '2016-11-18 13:45:34'),
(5, 1, 1, 'RGB-BIG', 1, '', '21', '22.708', '1200.00', '2016-10-24 23:54:51', '2016-11-18 12:59:49'),
(6, 1, 1, 'RGB-BIG', 0, '56', '', '', '1200.00', NULL, '2016-11-18 13:41:40'),
(7, 1, 1, 'RGB-BIG', 0, '66', '', '', '1200.00', NULL, '2016-11-18 12:32:48'),
(8, 1, 1, 'RGB-BIG', 0, '55', '', '', '1200.00', NULL, '2016-11-18 12:36:33'),
(10, 1, 1, 'RGB-BIG', 0, '67', '', '', '1200.00', NULL, '2016-11-18 12:59:12'),
(11, 1, 1, 'RGB-BIG', 0, '55', '', '', '1200.00', NULL, '2016-11-18 12:08:15'),
(14, 1, 1, 'RGB-BIG', 0, '39', '', '', '1000.00', NULL, '2016-11-18 12:55:37');

-- --------------------------------------------------------

--
-- Table structure for table `items_bak`
--

CREATE TABLE `items_bak` (
  `id` int(10) UNSIGNED NOT NULL,
  `users_id` int(11) NOT NULL,
  `categories_id` int(11) NOT NULL,
  `i_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_rgb` tinyint(4) NOT NULL,
  `qty` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `qty_bottle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `qty_content` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price_unit` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `items_bak`
--

INSERT INTO `items_bak` (`id`, `users_id`, `categories_id`, `i_name`, `is_rgb`, `qty`, `qty_bottle`, `qty_content`, `price_unit`, `created_at`, `updated_at`) VALUES
(3, 1, 1, 'RGB-BIG', 1, '', '30', '20', '1200.00', '2016-10-24 23:50:10', NULL),
(4, 1, 1, 'RGB-BIG', 1, '', '30', '20', '1200.00', '2016-10-24 23:52:54', NULL),
(5, 1, 1, 'RGB-BIG', 1, '', '30', '20', '1200.00', '2016-10-24 23:54:51', NULL),
(6, 1, 1, 'RGB-BIG', 0, '60', '', '', '1200.00', NULL, NULL),
(7, 1, 1, 'RGB-BIG', 0, '60', '', '', '1200.00', NULL, NULL),
(8, 1, 1, 'RGB-BIG', 0, '60', '', '', '1200.00', NULL, NULL),
(9, 1, 1, 'RGB-BIG', 0, '60', '', '', '1200.00', NULL, NULL),
(10, 1, 1, 'RGB-BIG', 0, '60', '', '', '1200.00', NULL, NULL),
(11, 1, 1, 'RGB-BIG', 0, '60', '', '', '1200.00', NULL, NULL),
(12, 1, 1, 'RGB-BIG', 0, '60', '', '', '1200.00', NULL, NULL),
(13, 1, 1, 'RGB-BIG', 0, '60', '', '', '1200.00', NULL, NULL),
(14, 1, 1, 'RGB-BIG', 0, '40', '', '', '1000.00', NULL, '2016-10-25 12:03:18');

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `migration` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`migration`, `batch`) VALUES
('2014_10_12_000000_create_users_table', 1),
('2014_10_12_100000_create_password_resets_table', 1),
('2016_10_10_213402_create_categories_table', 1),
('2016_10_10_213659_create_items_table', 1),
('2016_10_10_213810_create_purchase_logs_table', 1),
('2016_10_10_213911_create_sales_logs_table', 1),
('2016_10_10_215203_create_bottle_sales_table', 1),
('2016_10_10_215243_create_bottle_debtors_table', 1),
('2016_10_10_215443_create_pending_orders_table', 1),
('2016_10_10_215536_create_drivers_table', 1),
('2016_10_10_215612_create_suppliers_table', 1),
('2016_10_10_215638_create_customers_table', 1),
('2016_11_08_023115_create_carts_table', 2),
('2016_11_13_151640_create_cart_purchases_table', 3);

-- --------------------------------------------------------

--
-- Table structure for table `password_resets`
--

CREATE TABLE `password_resets` (
  `email` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `token` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `pending_orders`
--

CREATE TABLE `pending_orders` (
  `id` int(10) UNSIGNED NOT NULL,
  `store_users_id` int(11) NOT NULL,
  `sales_users_id` int(11) NOT NULL,
  `item_id` int(11) NOT NULL,
  `transaction_ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `d_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `i_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `c_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_rgb` tinyint(1) NOT NULL,
  `qty_content` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty_bottle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `qty` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `returned_qty` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `returned_bottle` varchar(255) COLLATE utf8_unicode_ci DEFAULT NULL,
  `price_unit` decimal(10,2) NOT NULL,
  `price_total` decimal(10,2) NOT NULL,
  `is_confirmed` tinyint(4) NOT NULL,
  `deleted` tinyint(1) DEFAULT '0',
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `pending_orders`
--

INSERT INTO `pending_orders` (`id`, `store_users_id`, `sales_users_id`, `item_id`, `transaction_ref`, `d_name`, `i_name`, `c_name`, `is_rgb`, `qty_content`, `qty_bottle`, `qty`, `returned_qty`, `returned_bottle`, `price_unit`, `price_total`, `is_confirmed`, `deleted`, `created_at`, `updated_at`) VALUES
(1, 1, 0, 4, '25579e8117d26b4e9WkjECioEVSeXnYD', 'Ola', '', 'Test', 1, '11', '30', '2', '', '', '1200.00', '2400.00', 0, 0, '2016-11-12 11:47:45', '2016-11-12 20:37:33'),
(2, 1, 0, 4, '25579e8117d26b4e9WkjECioEVSeXnYD', 'Ola', '', 'Test', 1, '9', '30', '5', '1', '4', '1200.00', '4800.00', 2, 0, '2016-11-12 11:47:45', '2016-11-12 20:41:28'),
(3, 1, 0, 14, '25579e8117d26b4e9WkjECioEVSeXnYD', 'Ola', '', 'Test', 0, NULL, NULL, '6', '2', NULL, '1000.00', '6000.00', 2, 0, '2016-11-12 13:46:04', '2016-11-12 20:41:28'),
(4, 0, 1, 5, '25579e81159d28d9bfpZLidSXZChLmjW', 'Individual', '', 'Test', 1, '20', '30', '4', NULL, NULL, '1200.00', '4800.00', 2, 0, '2016-11-13 11:16:30', '2016-11-13 11:27:47'),
(5, 0, 1, 6, '25579e81159d28d9bfpZLidSXZChLmjW', 'Individual', '', 'Test', 0, NULL, NULL, '6', NULL, NULL, '1200.00', '7200.00', 2, 0, '2016-11-13 11:16:30', '2016-11-13 11:27:47'),
(6, 0, 1, 14, '25579e81159d28d9bfpZLidSXZChLmjL', 'Individual', '', 'Test', 0, NULL, NULL, '3', NULL, NULL, '1000.00', '3000.00', 2, 0, '2016-11-13 11:29:14', '2016-11-13 13:23:32'),
(7, 0, 1, 11, '25579e8117d26b4e9EAUqfokeAnqYdXj', 'Individual', '', 'Test', 0, NULL, NULL, '2', NULL, NULL, '1200.00', '2400.00', 2, 0, '2016-11-13 12:22:29', '2016-11-17 13:21:37'),
(8, 0, 1, 4, '25579e81158c0b166bbaTdhiUZmToFbF', 'Individual', '', 'Test', 1, '5', '34', '2', NULL, NULL, '1200.00', '2400.00', 2, 0, '2016-11-13 13:25:40', '2016-11-13 13:27:12'),
(9, 1, 0, 5, '25579e81183da3647oekbEgbWWdVLmgg', 'Ola', '', 'Test', 1, '15.708', '30', '4', '1', '3', '1200.00', '3600.00', 1, 0, '2016-11-16 05:42:39', '2016-11-18 12:32:24'),
(10, 0, 1, 4, '25579e811d4fcff62nqYmLCXAeqUApRh', 'Individual', '', 'Test', 1, '6', '34', '1', NULL, NULL, '1200.00', '1200.00', 2, 0, '2016-11-16 05:48:57', '2016-11-18 12:42:27'),
(11, 1, 0, 3, '25579e8117d26b4e9qjhcobeohpRaidD', 'Ola', '', 'Test', 1, '30', '18', '2', '0', '2', '1200.00', '2400.00', 2, 0, '2016-11-17 21:09:58', '2016-11-18 12:12:58'),
(12, 1, 0, 5, '25579e8117d26b4e9qjhcobeohpRaidD', 'Ola', '', 'Test', 1, '11.708', '30', '3', '0', '3', '1200.00', '3600.00', 2, 0, '2016-11-17 21:09:58', '2016-11-18 12:12:58'),
(13, 1, 0, 11, '25579e8117d26b4e9qjhcobeohpRaidD', 'Ola', '', 'Test', 0, NULL, NULL, '4', '1', NULL, '1200.00', '3600.00', 2, 0, '2016-11-17 21:09:58', '2016-11-18 12:12:58'),
(14, 1, 0, 14, '25579e8116db6b86aapTTCfiBTDbgSRe', 'Ola', '', 'Test', 0, NULL, NULL, '3', '0', NULL, '1000.00', '3000.00', 2, 0, '2016-11-18 12:30:14', '2016-11-18 12:35:20'),
(15, 1, 0, 7, '25579e8116db6b86aapTTCfiBTDbgSRe', 'Ola', '', 'Test', 0, NULL, NULL, '4', '0', NULL, '1200.00', '4800.00', 2, 0, '2016-11-18 12:30:14', '2016-11-18 12:35:20'),
(16, 0, 1, 8, '25579e8112a30f393RDaagBkikAmBDcS', 'Individual', '', 'Test', 0, NULL, NULL, '5', NULL, NULL, '1200.00', '6000.00', 2, 0, '2016-11-18 12:38:00', '2016-11-18 12:41:25'),
(17, 0, 1, 5, '25579e8112a30f393RDaagBkikAmBDcS', 'Individual', '', 'Test', 1, '9.708', '36', '2', NULL, NULL, '1200.00', '2400.00', 2, 0, '2016-11-18 12:38:01', '2016-11-18 12:41:25');

-- --------------------------------------------------------

--
-- Table structure for table `purchase_logs`
--

CREATE TABLE `purchase_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `item_id` int(11) NOT NULL,
  `transaction_ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `s_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `i_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_rgb` tinyint(4) DEFAULT '0',
  `is_bottle` tinyint(4) DEFAULT '0',
  `no_exchange` tinyint(4) DEFAULT '0',
  `qty` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `qty_bottle` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `price_unit` decimal(10,2) NOT NULL,
  `price_total` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `purchase_logs`
--

INSERT INTO `purchase_logs` (`id`, `item_id`, `transaction_ref`, `s_name`, `i_name`, `is_rgb`, `is_bottle`, `no_exchange`, `qty`, `qty_bottle`, `price_unit`, `price_total`, `created_at`, `updated_at`) VALUES
(1, 3, '25579e811a88e8808miVTXALLoGDDnpa', 'Ibk Stores', '', 1, 0, 1, '2', '', '1200.00', '2400.00', '2016-11-14 08:17:22', '2016-11-14 08:17:22'),
(2, 10, '25579e811a88e8808miVTXALLoGDDnpa', 'Ibk Stores', '', 0, 0, NULL, '3', '', '1200.00', '3600.00', '2016-11-14 08:17:22', '2016-11-14 08:17:22'),
(3, 3, '25579e81139293b9ckDEZqABUYDFDXnR', 'Ibk Stores', '', 1, 1, 0, '2', '', '1000.00', '2000.00', '2016-11-14 14:56:25', '2016-11-14 14:56:25'),
(4, 3, '25579e8119a92ddcdBGFkCkpAUpoDCAV', 'Ibk Stores', '', 1, 0, 1, '2', '', '1200.00', '2400.00', '2016-11-16 05:57:16', '2016-11-16 05:57:16'),
(5, 5, '25579e811e5311062dnWaXRAFXhmfABa', 'Ibk Stores', '', 1, 0, 1, '12', '', '1200.00', '14400.00', '2016-11-18 12:47:45', '2016-11-18 12:47:45'),
(6, 4, '25579e811e5311062dnWaXRAFXhmfABa', 'Ibk Stores', '', 1, 0, 1, '4', '', '1200.00', '4800.00', '2016-11-18 12:47:46', '2016-11-18 12:47:46'),
(7, 14, '25579e8111f3f7b95qVjDajCLBpZnTAR', 'Ibk Stores', '', 0, 0, NULL, '2', '', '1000.00', '2000.00', '2016-11-18 12:55:59', '2016-11-18 12:55:59'),
(8, 10, '25579e811d2c2f186BqbTBUFqEiqqEXF', 'Ibk Stores', '', 0, 0, NULL, '4', '', '1200.00', '4800.00', '2016-11-18 13:00:15', '2016-11-18 13:00:15'),
(9, 5, '25579e811d2c2f186BqbTBUFqEiqqEXF', 'Ibk Stores', '', 1, 0, 1, '3', '', '1200.00', '3600.00', '2016-11-18 13:00:15', '2016-11-18 13:00:15'),
(10, 6, '25579e8116db6b86aGjRobCUBVjbUZEi', 'Ibk Stores', '', 0, 0, NULL, '2', '', '1200.00', '2400.00', '2016-11-18 13:42:35', '2016-11-18 13:42:35'),
(11, 4, '25579e811d4fcff62XfGUpgATqoaRWST', 'Ibk Stores', '', 1, 0, 1, '2', '', '1200.00', '2400.00', '2016-11-18 13:47:16', '2016-11-18 13:47:16');

-- --------------------------------------------------------

--
-- Table structure for table `sales_logs`
--

CREATE TABLE `sales_logs` (
  `id` int(10) UNSIGNED NOT NULL,
  `users_id` int(11) NOT NULL,
  `d_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `transaction_ref` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `qty` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `total` decimal(10,2) NOT NULL,
  `amount_paid` decimal(10,2) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `sales_logs`
--

INSERT INTO `sales_logs` (`id`, `users_id`, `d_name`, `transaction_ref`, `qty`, `total`, `amount_paid`, `created_at`, `updated_at`) VALUES
(1, 1, 'Ola', '25579e8117d26b4e9WkjECioEVSeXnYD', '', '13200.00', '13200.00', '2016-11-12 20:41:28', '2016-11-12 20:41:28'),
(2, 1, 'Individual', '25579e81159d28d9bfpZLidSXZChLmjW', '', '12000.00', '12000.00', '2016-11-13 11:27:47', '2016-11-13 11:27:47'),
(3, 1, 'Individual', '25579e81159d28d9bfpZLidSXZChLmjL', '', '3000.00', '3000.00', '2016-11-13 13:23:32', '2016-11-13 13:23:32'),
(4, 1, 'Individual', '25579e81158c0b166bbaTdhiUZmToFbF', '', '2400.00', '2400.00', '2016-11-13 13:27:12', '2016-11-13 13:27:12'),
(5, 1, 'Individual', '25579e8117d26b4e9EAUqfokeAnqYdXj', '', '2400.00', '2400.00', '2016-11-17 13:21:37', '2016-11-17 13:21:37'),
(6, 1, 'Ola', '25579e8117d26b4e9qjhcobeohpRaidD', '', '9600.00', '9600.00', '2016-11-18 12:12:57', '2016-11-18 12:12:57'),
(7, 1, 'Ola', '25579e8116db6b86aapTTCfiBTDbgSRe', '', '7800.00', '7800.00', '2016-11-18 12:35:20', '2016-11-18 12:35:20'),
(8, 1, 'Individual', '25579e8112a30f393RDaagBkikAmBDcS', '', '8.00', '8400.00', '2016-11-18 12:41:25', '2016-11-18 12:41:25'),
(9, 1, 'Individual', '25579e811d4fcff62nqYmLCXAeqUApRh', '', '1.00', '1200.00', '2016-11-18 12:42:27', '2016-11-18 12:42:27');

-- --------------------------------------------------------

--
-- Table structure for table `suppliers`
--

CREATE TABLE `suppliers` (
  `id` int(10) UNSIGNED NOT NULL,
  `s_name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `phone` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `address` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `suppliers`
--

INSERT INTO `suppliers` (`id`, `s_name`, `phone`, `address`, `created_at`, `updated_at`) VALUES
(1, 'Ibk Stores', '2348038021677', 'Abeokuta, Ogun State', '2016-10-27 16:44:55', '2016-10-27 16:49:34');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(10) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `password` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `level` tinyint(4) NOT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `name`, `password`, `level`, `created_at`, `updated_at`) VALUES
(1, 'admin', 'admin', 1, NULL, NULL),
(2, 'test', 'test123', 2, '2016-10-15 14:26:46', '2016-10-27 18:05:17'),
(3, 'ehis', '123456', 0, '2016-10-15 15:40:15', '2016-10-15 15:40:15');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `bottle_debtors`
--
ALTER TABLE `bottle_debtors`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `bottle_sales`
--
ALTER TABLE `bottle_sales`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `carts`
--
ALTER TABLE `carts`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `cart_purchases`
--
ALTER TABLE `cart_purchases`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `customers`
--
ALTER TABLE `customers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `drivers`
--
ALTER TABLE `drivers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items`
--
ALTER TABLE `items`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `items_bak`
--
ALTER TABLE `items_bak`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `password_resets`
--
ALTER TABLE `password_resets`
  ADD KEY `password_resets_email_index` (`email`),
  ADD KEY `password_resets_token_index` (`token`);

--
-- Indexes for table `pending_orders`
--
ALTER TABLE `pending_orders`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `purchase_logs`
--
ALTER TABLE `purchase_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `sales_logs`
--
ALTER TABLE `sales_logs`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `suppliers`
--
ALTER TABLE `suppliers`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `bottle_debtors`
--
ALTER TABLE `bottle_debtors`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `bottle_sales`
--
ALTER TABLE `bottle_sales`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `carts`
--
ALTER TABLE `carts`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `cart_purchases`
--
ALTER TABLE `cart_purchases`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
--
-- AUTO_INCREMENT for table `customers`
--
ALTER TABLE `customers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `drivers`
--
ALTER TABLE `drivers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `items`
--
ALTER TABLE `items`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `items_bak`
--
ALTER TABLE `items_bak`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
--
-- AUTO_INCREMENT for table `pending_orders`
--
ALTER TABLE `pending_orders`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=18;
--
-- AUTO_INCREMENT for table `purchase_logs`
--
ALTER TABLE `purchase_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;
--
-- AUTO_INCREMENT for table `sales_logs`
--
ALTER TABLE `sales_logs`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `suppliers`
--
ALTER TABLE `suppliers`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;