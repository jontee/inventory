
        <div class="modal fade" id="helpModal" tabindex="-1" role="dialog" aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content bbn-modal">
                    <form method="post" id="compose-modal">
                        <div class="modal-header text-left">
                            <div class="text-right">
                                <button class="glyphicon glyphicon-remove" data-dismiss="modal" style="border:0px;"></button>
                            </div>
                            <div class="text-left">
                                <h4 class="modal-title">Help</h4>
                            </div>
                        </div>
                        <div class="modal-body printable">
                            <div class="well">
                                <p>
                                    Navigate through the app using the menu on the side bar
                                </p>
                                <div class="list-group">
                                    <h4 class="list-group-item-heading">Admin Control</h4>
                                    <p class="list-group-item-text">This is where you can find administrative tools like Adding Items, Users, Suppliers</p>
                                </div>

                                <div class="list-group">
                                    <h4 class="list-group-item-heading">Report</h4>
                                    <p class="list-group-item-text">
                                        This is an interesting feature for monitoring your stock and sales
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="modal-footer" style="float:none;">

                            <!-- <a href="javascript:window.print()" class="btn btn-success">
                                                <i class="fa fa-print pr5"></i> Print</a> -->
                            <button type="button" class="btn btn-modal-save pull-left"
                                    data-dismiss="modal" >Close</button>
                        </div>
                    </form>
                    </div>
                </div>
            </div>